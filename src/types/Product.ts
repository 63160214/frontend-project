export default interface Product {
  id?: number;
  name: string;
  price: number;
  creataAt?: Date;
  updateAt?: Date;
  deleteAt?: Date;
}
