export default interface Product {
  id?: number;
  name: string;
  phone: string;
  creataAt?: Date;
  updateAt?: Date;
  deleteAt?: Date;
}
